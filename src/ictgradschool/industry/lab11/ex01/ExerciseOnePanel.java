package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calCulateHealthBMIBotton;
    private  JTextField Height;
    private  JTextField Weight;
    private  JTextField IBM;
    private  JTextField HealthIBM;


    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        JLabel height = new JLabel("Height in metres: ");
        this.add(height);

        Height = new JTextField(15);
        this.add(Height);

        JLabel weight = new JLabel("Weight in kilograms: ");
        this.add(weight);

        Weight = new JTextField(15);
        this.add(Weight);

        this.calculateBMIButton = new JButton("Calculate BMI");
        this.add(calculateBMIButton);
        calculateBMIButton.addActionListener(this);


        JLabel yourIBM = new JLabel("Your Body Mass Index(BMI) is:");
        this.add(yourIBM);

        IBM = new JTextField(15);
        this.add(IBM);

        this.calCulateHealthBMIBotton = new JButton("Calculate Health BMI Weight");
        this.add(calCulateHealthBMIBotton);
        calCulateHealthBMIBotton.addActionListener(this);

        JLabel healthWeight = new JLabel("Maximum Healthy Weight for your Height:");
        this.add(healthWeight);

        HealthIBM = new JTextField(15);
        this.add(HealthIBM);







        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        // TODO Add Action Listeners for the JButtons

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        double height = 0.00;
        double weight = 0.00;
        try {
            height = Double.parseDouble(Height.getText());
            weight = Double.parseDouble(Weight.getText());
        }
        catch (NumberFormatException e)
        {
            System.out.println("Did not enter either valid height or weight value.");
        }

        if (event.getSource() == calculateBMIButton) {

            double BMI = roundTo2DecimalPlaces(weight/(height*height));
            IBM.setText(""+BMI);

        }
        else if (event.getSource() == calCulateHealthBMIBotton) {
            double HealthWeight = roundTo2DecimalPlaces(24.9*height*height);
            HealthIBM.setText(""+HealthWeight);
        }
    }




    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}