package ictgradschool.industry.lab11.ex04;

import ictgradschool.industry.lab11.examples.example7.E7Shape;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private List<Balloon> balloons;
    private  JButton moveButton;
    private  Timer timer;
    private Timer reproduction;


    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);
        this.balloons = new ArrayList<Balloon>();

        Balloon balloon = new Balloon(30, 60);
        balloons.add(balloon);
        //this.moveButton = new JButton("Move balloon");
        //this.moveButton.addActionListener(this);
        //this.add(moveButton);
        this.addKeyListener(this);
        this.timer = new Timer(200, this);
        this.reproduction= new Timer (1000, this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == timer) {
            for (Balloon balloon: balloons) {
                balloon.move();
                requestFocusInWindow();
                repaint();
            }
            // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
            // events even after we've clicked the button.
        }
        if(e.getSource() == reproduction)
        {
            balloons.add(new Balloon((int)(Math.random()*100), (int)(Math.random()*100)));
            repaint();
        }
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Balloon balloon: balloons) {
            balloon.draw(g);
        }
        requestFocusInWindow();
    }

    @Override
    public void keyPressed(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.VK_R)
        {
            if(reproduction.isRunning())
            {
                reproduction.stop();
            }
            else {
                reproduction.start();
            }
        }
        for (Balloon balloon: balloons) {

            if (timer.isRunning()) {
                switch (event.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        balloon.setDirection(Direction.Up);
                        break;
                    case KeyEvent.VK_DOWN:
                        balloon.setDirection(Direction.Down);
                        break;
                    case KeyEvent.VK_RIGHT:
                        balloon.setDirection(Direction.Right);
                        break;
                    case KeyEvent.VK_LEFT:
                        balloon.setDirection(Direction.Left);
                        break;
                    case KeyEvent.VK_S:
                        timer.stop();
                }
            } else {
                switch (event.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        balloon.setDirection(Direction.Up);
                        timer.start();
                        break;
                    case KeyEvent.VK_DOWN:
                        balloon.setDirection(Direction.Down);
                        timer.start();
                        break;
                    case KeyEvent.VK_RIGHT:
                        balloon.setDirection(Direction.Right);
                        timer.start();
                        break;
                    case KeyEvent.VK_LEFT:
                        balloon.setDirection(Direction.Left);
                        timer.start();
                        break;
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e){

    }
    @Override
    public void keyTyped(KeyEvent e){

    }


}