package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{

    /**
     * Creates a new ExerciseFivePanel.
     */
    private JButton add;
    private JButton subtract;
    private  JTextField firstNum;
    private  JTextField secondNum;
    private  JTextField ans;


    public ExerciseTwoPanel() {
        setBackground(Color.white);

        firstNum = new JTextField(10);
        this.add(firstNum);

        secondNum = new JTextField(10);
        this.add(secondNum);


        add = new JButton("Add");
        this.add(add);
        add.addActionListener(this);

        subtract = new JButton("Subtract");
        this.add(subtract);
        add.addActionListener(this);

        JLabel answer = new JLabel("Result: ");
        this.add(answer);

        ans = new JTextField( 10);
        this.add(ans);

    }

    @Override

    public void actionPerformed(ActionEvent event) {
        double first = 0.00;
        double second = 0.00;
        try {
            first = Double.parseDouble(firstNum.getText());
            second = Double.parseDouble(secondNum.getText());
        } catch (NumberFormatException e) {
            System.out.println("Did not enter either valid value/values.");
        }

        if (event.getSource() == add) {
            double result = roundTo2DecimalPlaces((first + second));
            ans.setText(String.valueOf(result));
        } else if (event.getSource() == subtract) {
            double result = roundTo2DecimalPlaces((first - second));
            ans.setText(String.valueOf(result));
        }
    }



        /**
         * A library method that rounds a double to 2dp
         *
         * @param amount to round as a double
         * @return the amount rounded to 2dp
         */
    private double roundTo2DecimalPlaces (double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}