package ictgradschool.industry.lab11.ex03;

import javax.sound.sampled.Line;
import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // TODO Draw a house, as shown in the lab handout.

       int unit = size*1;
       int x = left;
       int y = top;
       Polygon big = new Polygon(new int[]{x, x+10*unit, x+10*unit, x}, new int[]{y+5*unit, y+5*unit, y+12*unit, y+12*unit},4);
       Polygon window1 = new Polygon(new int[]{x+1*unit, x+3*unit, x+3*unit, x+1*unit}, new int[]{y+7*unit, y+7*unit, y+9*unit, y+9*unit},4);
       Polygon window2 = new Polygon(new int[]{x+7*unit, x+9*unit, x+9*unit, x+7*unit}, new int[]{y+7*unit, y+7*unit, y+9*unit, y+9*unit},4);
       Polygon roof = new Polygon(new int[]{x, x+5*unit, x+10*unit}, new int[]{y+5*unit, y, y+5*unit},3);
        Polygon door = new Polygon(new int[]{x+4*unit, x+6*unit, x+6*unit, x+4*unit}, new int[]{y+8*unit, y+8*unit, y+12*unit, y+12*unit}, 4);
        Polygon roofSmall = new Polygon(new int[]{x+7*unit, x+8*unit, x+8*unit, x+7*unit}, new int[]{y+1*unit, y+1*unit, y+3*unit, y+2*unit},4);
        g.setColor(MAIN_COLOR);
        g.fillPolygon(big);
        g.setColor(WINDOW_COLOR);
       g.fillPolygon(window1);
        g.fillPolygon(window2);
        g.setColor(DOOR_COLOR);
        g.fillPolygon(door);
        g.setColor(ROOF_COLOR);
        g.fillPolygon(roof);
        g.setColor(CHIMNEY_COLOR);
        g.fillPolygon(roofSmall);
        g.setColor(Color.black);
        g.drawPolygon(big);
        g.drawPolygon(window1);
        g.drawPolygon(window2);
       g.drawLine(x+2*unit, y+7*unit, x+2*unit, y+9*unit);
       g.drawLine(x+unit, y+8*unit, x+3*unit, y+8*unit);
       g.drawLine(x+8*unit,y+7*unit, x+8*unit, y+9*unit);
        g.drawLine(x+7*unit, y+8*unit, x+9*unit, y+8*unit);
        g.drawPolygon(roof);
        g.drawPolygon(door);
        g.drawPolygon(roofSmall);



//g.fillPolygon(big).;





    }
}